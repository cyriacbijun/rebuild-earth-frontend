import React from 'react';
import {
  Link
} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grow from '@material-ui/core/Grow';

const useStyles = makeStyles({
  card: {
    maxWidth: 345,
    margin: "30px 35px"
  },
  media: {
    height: 140,
    margin: "auto"
  },
  actionArea: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column"
  },
});

function Option(props) {
  const classes = useStyles();

  return (
    <Grow in={true} style={{ transformOrigin: '0 0 0' }}>

      <Link to={props.route} style={{ textDecoration: "none"}}>
        <Card className={classes.card}>
          <CardActionArea className={classes.actionArea}>
            <img
              className={classes.media}
              src={props.logo}
              title={props.name}
              alt={props.name}
              />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {props.name}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                {props.description}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Link>
      
    </Grow>
  );
}

export default Option;
