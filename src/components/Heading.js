import React from 'react';

import Typography from '@material-ui/core/Typography';

function RequestRescue(props) {
  return (
    <div style={{margin: "40px 0 60px 0", width: "100%"}}>
      <Typography variant="h2" component="h2" style={{textAlign: "center", width: "100%", fontSize: "3.5rem"}}>
        {props.value}
      </Typography>
    </div>
  );
}

export default RequestRescue;
