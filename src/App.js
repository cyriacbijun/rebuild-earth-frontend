import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
// pages
import Home from './pages/Home'
import Login from './pages/Login'
import Contribute from './pages/Contribute'
import ReliefCamps from './pages/ReliefCamps'
import RequestRescue from './pages/RequestRescue'
import NotFound from './pages/NotFound'

function App() {

  return (
    <div className="App" style={styles.app}>
      <Router>
      <div>
        <p>
        <Link to="/">Home</Link>
        <Link to="/login">Login</Link>
        </p>
        {/* 
            Add your routes here
            the first matched route will be used
        */}
        <Switch>

          <Route exact path="/" component={Home} />

          <Route exact path="/login" component={Login} />

          <Route exact path="/contribute" component={Contribute} />

          <Route path="/request-rescue" exact component={RequestRescue} />

          <Route path="/relief-camps" exact component={ReliefCamps} />

          <Route component={NotFound} />
          
        </Switch>
      </div>
    </Router>
    </div>
  );
}

export default App;

const styles = {
  app: {
    display: "flex",
    margin: 0,
    minHeight: "100vh",
  }
}
