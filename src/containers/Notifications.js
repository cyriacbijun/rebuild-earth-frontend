import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import './notifications.css'

const useStyles = makeStyles({
  card: {
    position:'absolute',
    right:0,
    
    // minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
      marginTop:'-16px',
      marginLeft:'-16px',
      paddingLeft:'10px',
      paddingTop:'10px',
      backgroundColor:'rgb(228, 225, 225)',
    fontSize: 20,color:'black'
  },
  pos: {
    marginBottom: 12,
  }
});

export default function Notifications() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={[classes.card, "notification-card"]}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          Notifications
        </Typography>
        <ul className="notification-list-ul">
            <li className="notification-list-li">Notification1</li>
            <li className="notification-list-li">Notification 2</li>
        </ul>
      </CardContent>
      <CardActions>
        <Button size="small">Learn More</Button>
      </CardActions>
    </Card>
  );
}