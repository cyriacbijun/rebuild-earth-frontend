
import React from 'react';

import Option from '../components/Option';  

function HomePageOptions() {
  return (
    <div style={styles.homeOptions}>
      <Option route="/request-rescue" name="Request Rescue" description="Request for a rescue operation from  your location. Some easy steps for fast help" logo="https://image.flaticon.com/icons/svg/64/64392.svg" />
      <Option route="/contribute" name="Contribute" description="Do what you can. Donate to the relief fund for the diaster."  logo="https://image.flaticon.com/icons/svg/1692/1692130.svg" />
      <Option route="/relief-camps" name="Relief Camps" description="Get the details of relief camps near you. Go there for shelter if in danger." logo="https://image.flaticon.com/icons/svg/1064/1064799.svg" />
      <Option route="/request-rescue" name="Request Rescue" description="Request for a rescue operation from  your location. Some easy steps for fast help" logo="https://image.flaticon.com/icons/svg/64/64392.svg" />
      <Option route="/contribute" name="Contribute" description="Do what you can. Donate to the relief fund for the diaster."  logo="https://image.flaticon.com/icons/svg/1692/1692130.svg" />
      <Option route="/relief-camps" name="Relief Camps" description="Get the details of relief camps near you. Go there for shelter if in danger." logo="https://image.flaticon.com/icons/svg/1064/1064799.svg" />
      <Option route="/request-rescue" name="Request Rescue" description="Request for a rescue operation from  your location. Some easy steps for fast help" logo="https://image.flaticon.com/icons/svg/64/64392.svg" />
      <Option route="/contribute" name="Contribute" description="Do what you can. Donate to the relief fund for the diaster."  logo="https://image.flaticon.com/icons/svg/1692/1692130.svg" />
      <Option route="/relief-camps" name="Relief Camps" description="Get the details of relief camps near you. Go there for shelter if in danger." logo="https://image.flaticon.com/icons/svg/1064/1064799.svg" />
      <Option route="/request-rescue" name="Request Rescue" description="Request for a rescue operation from  your location. Some easy steps for fast help" logo="https://image.flaticon.com/icons/svg/64/64392.svg" />
    </div>
  );
}

export default HomePageOptions;

const styles = {
  homeOptions: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    margin: "0 5%"
  }
}