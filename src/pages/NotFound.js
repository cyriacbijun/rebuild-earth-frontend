import React from 'react';

import Heading from '../components/Heading'

function NotFound() {
  return (
    <div>
      <Heading value="Not Found" />
    </div>
  );
}

export default NotFound;