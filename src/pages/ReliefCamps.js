import React from 'react';

import Heading from '../components/Heading'

function ReliefCamps() {
  return (
    <div>
      <Heading value="Relief Camps Page" />
    </div>
  );
}

export default ReliefCamps;
