import React, {useEffect} from 'react';
import Notifications from '../containers/Notifications';
import ReactNotifications from 'react-notifications-component';
import { store } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import 'animate.css';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import axios from 'axios';

import Heading from '../components/Heading'

import HomePageOptions from '../containers/HomePageOptions';

function Home() {

    useEffect(() => {
        axios
            .get(
                "https://api.rebuildearth.org/api/news/?limit=4&offset=0"
            )
            .then(({ data }) => {
                data.results.map((results)=>{
                    store.addNotification({
                        title: 'Important Notifications',
                        content: results.title,
                        type: 'info',                         // 'default', 'success', 'info', 'warning'
                        container: 'top-center',                // where to position the notifications
                        animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
                        animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
                        dismiss: {
                            duration: 5000
                        }
                    });

                });
            });
    }, []);

    return (
    <div className="App">
    <Notifications/>
        <Heading value="Home Page" />
        <ReactNotifications />
        <p>
            Home page
        </p>
        <HomePageOptions />
    </div>
  );
}

export default Home;