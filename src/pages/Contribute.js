import React from 'react';

import Heading from '../components/Heading'

function Contribute() {
  return (
    <div>
      <Heading value="Contribute Page" />
    </div>
  );
}

export default Contribute;
